import requests


def get_json(url):
    r = requests.get(url)
    data = r.json()
    return data


def get_trait(trait, include_url=False):
    trait = trait.lower()
    url = "https://temtem-api.mael.tech/api/traits"
    data = get_json(url)
    for i in range(len(data)):
        if data[i]['name'].lower() == trait:
            trait_info = data[i]
            reply = "\n" + trait_info['description']
            if include_url:
                reply += "\n" + trait_info['wikiUrl']
            return [trait_info['name'], reply]
    return False


def get_details(details):
    reply = "\nHeight: " + str(details['height']['cm']) + " cm/"
    reply += str(details['height']['inches']) + " inches"
    reply += "\nWeight: " + str(details['weight']['kg']) + " kg/"
    reply += str(details['weight']['lbs']) + " lbs"
    return reply


def get_evolution(evo_info):
    reply = "\nStages: " + str(len(evo_info['evolutionTree']))
    for evo in evo_info['evolutionTree']:
        reply += "\n" + evo['name']
        try:
            reply += ", " + str(evo['levels']) + " levels to evolve"
        except KeyError:
            pass
    return reply


def get_techniques(tech_info):
    reply = ""
    for tech in tech_info:
        reply += "\n- " + tech['name']
        source = tech['source']
        if source == "Levelling":
            reply += "\nLevel " + str(tech['levels'])
        elif source == "Breeding":
            reply += "\nBreeding"
        elif source == "TechniqueCourses":
            reply += "\nTechnique course"

    return reply


def get_locations(loc_info):
    reply = ""
    for loc in loc_info:
        reply += "\n- " + loc['location']
        reply += "\nLevel range: " + loc['level']
        reply += "\nFrequency: " + loc['frequency']
    return reply



def get_items(item):
    url = "https://temtem-api.mael.tech/api/items"
    data = get_json(url)
    for i in range(len(data)):
        if data[i]['name'].lower() == item:
            item_data = data[i]

            # If item is a course, return false and use get_course instead
            if item_data['category'] == "course":
                return False

            reply = "Category: " + item_data['category']
            if item_data['description']:
                reply += "\nDescription: " + item_data['description']
            if item_data['effect']:
                reply += "\nEffect: " + item_data['effect']
            reply += "\nBuy price: " + str(item_data['buyPrice'])
            reply += "\nSell price: " + str(item_data['sellPrice'])
            reply += "\nWiki url: " + item_data['wikiUrl']
            return [item_data['name'], reply]
    return False


def get_gear(gear):
    url = "https://temtem-api.mael.tech/api/gear"
    data = get_json(url)
    for i in range(len(data)):
        if data[i]['name'].lower() == gear:
            gear_data = data[i]
            reply = "Description: " + gear_data['gameDescription']
            reply += "\nCategory: " + gear_data['category']
            reply += "\nConsumable: " + str(gear_data['consumable'])
            reply += "\nLimited Quantity: " + str(gear_data['limitedQuantity'])
            if gear_data['buyPrice'] != 0:
                reply += "\nBuy price: " + str(gear_data['buyPrice']) + "k"
            reply += "\nWiki url: " + gear_data['wikiUrl']
            return [gear_data['name'], reply]
    return False


def get_course(course):
    url = "https://temtem-api.mael.tech/api/training-courses"
    data = get_json(url)
    for i in range(len(data)):
        if data[i]['technique'].lower() == course:
            course_data = data[i]
            reply = "Type: " + course_data['type']
            reply += "\nNumber: " + course_data['number']
            reply += "\nLocation: " + course_data['location']
            reply += "\nQuest/Found: " + course_data['locationType']
            return [course_data['technique'], reply]
    return False


def get_help(discord, prefix):
    embed = discord.Embed(title="Help",
                          description="Here is a list of possible commands and what parameters they take\n\n"
                                      "Parameters given in brackets [like this] must be exchanged with desired "
                                      "information, and are required. Other parameters are built in commands "
                                      "and they are optional",
                          color=0x00d0f5)

    embed.add_field(name=prefix + "temtem",
                    value="[temtem]/[number]\n"
                          "luma, stats, details, techniques/moves, trivia, evolution/evo, locations/loc, traits",
                    inline=False)

    embed.add_field(name=prefix + "freetem",
                    value="[temtem], [level]",
                    inline=False)

    embed.add_field(name=prefix + "temquest",
                    value="[quest]",
                    inline=False)

    embed.add_field(name=prefix + "temitem",
                    value="[item]",
                    inline=False)

    embed.add_field(name=prefix + "temgear",
                    value="[gear]",
                    inline=False)

    embed.add_field(name=prefix + "temcourse",
                    value="[technique]",
                    inline=False)

    embed.add_field(name=prefix + "temtrait",
                    value="[trait]",
                    inline=False)

    embed.add_field(name=prefix + "tempatch",
                    value="Returns a link to the latest official patch notes",
                    inline=False)

    embed.add_field(name="Example",
                    value=prefix + "temtem Raican stats luma\n\n"
                                   "This command returns information about Raican including it's stats "
                                   "and the luma version of his picture",
                    inline=False)
    return embed
