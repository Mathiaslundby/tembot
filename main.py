import discord
from functions import *
from discord.ext import commands

token = "NzY4ODM1MTkwMTA5NzY1NjU2.X5GPkQ.OSd8y67ZSIOxmIyl3VcEi3h3pTc"
# bot = discord.Client()
bot = commands.Bot(command_prefix='*')


@bot.command()
async def temtem(ctx, *args):
    luma = False
    stats = False
    details = False
    techniques = False
    trivia = False
    evolution = False
    locations = False
    traits = False

    for arg in args:
        arg = arg.lower()
        if arg == "luma":
            luma = True
        if arg == 'stats':
            stats = True
        if arg == 'details':
            details = True
        if arg == 'techniques' or arg == "moves":
            techniques = True
        if arg == 'trivia':
            trivia = True
        if arg == "evolution" or arg == "evo":
            evolution = True
        if arg == "locations" or arg == "loc":
            locations = True
        if arg == "traits":
            traits = True

    tem_info = {}
    try:
        num = int(args[0])
        url = "https://temtem-api.mael.tech/api/temtems/" + str(num)
        tem_info = get_json(url)
    except ValueError:
        name = args[0]
        url = "https://temtem-api.mael.tech/api/temtems"
        data = get_json(url)
        for i in range(len(data)):
            if data[i]['name'].lower() == name:
                tem_info = data[i]

    if len(tem_info) == 0:
        await ctx.send(":x: **Could not find temtem**")
        return


    desc = "\nNumber: " + str(tem_info['number'])
    desc += "\nTypes: "
    for t in tem_info['types']:
        desc += t + ", "
    desc = desc[:len(desc) - 2]
    desc += "\nGender ratio: "
    desc += str(tem_info['genderRatio']['male']) + "% Male, "
    desc += str(tem_info['genderRatio']['female']) + "% Female"
    desc += "\nCatch rate: " + str(tem_info['catchRate'])
    desc += "\nHatch time: " + str(tem_info['hatchMins']) + " minutes"

    tv_yields = tem_info['tvYields']
    desc += "\nTV yields: "
    for tv in tv_yields:
        if tv_yields[tv] != 0:
            desc += str(tv_yields[tv]) + " " + tv + ", "
    desc = desc[:len(desc) - 2]

    embed = discord.Embed(title=tem_info['name'],
                          description=desc,
                          color=0x00d0f5)

    reply = ""
    if details:
        value = get_details(tem_info['details'])
        embed.add_field(name="Details", value=value, inline=False)

    if trivia:
        value = ""
        for triv in tem_info['trivia']:
            value += "\n" + triv
        embed.add_field(name="Trivia", value=value, inline=False)

    if evolution:
        value = ""
        if tem_info['evolution']['evolves']:
            value += get_evolution(tem_info['evolution'])
            embed.add_field(name="Evolution", value=value, inline=False)

    if stats:
        value = ""
        stats_info = tem_info['stats']
        for stat in stats_info:
            value += "\n" + stat + ": " + str(stats_info[stat])
        embed.add_field(name="Stats", value=value, inline=False)

    if traits:
        trait_info = tem_info['traits']
        value = ""
        for trait in trait_info:
            t = get_trait(trait)
            value += "\n- " + t[0]
            value += t[1]
        embed.add_field(name="Traits", value=value, inline=False)

    if locations:
        loc_info = tem_info['locations']
        value = ""
        if len(loc_info) > 0:
            value += get_locations(loc_info)
            embed.add_field(name="Locations", value=value, inline=False)


    if techniques:
        tech_info = tem_info['techniques']
        value = get_techniques(tech_info)
        embed.add_field(name="Techniques", value=value, inline=False)

    if luma:
        if tem_info['wikiRenderAnimatedLumaUrl'] != "":
            embed.set_image(url=tem_info['wikiRenderAnimatedLumaUrl'])
        else:
            embed.set_image(url=tem_info['lumaWikiPortraitUrlLarge'])
    else:
        if tem_info['wikiRenderAnimatedLumaUrl'] != "":
            embed.set_image(url=tem_info['wikiRenderAnimatedUrl'])
        else:
            embed.set_image(url=tem_info['wikiPortraitUrlLarge'])

    await ctx.send(embed=embed)


@bot.command()
async def freetem(ctx, tem, lvl):
    url = "https://temtem-api.mael.tech/api/freetem/" + tem + "/" + lvl
    data = get_json(url)

    reply = "Level: " + str(data['level'])
    reply += "\nCatch rate: " + str(data['catchRate'])
    reply += "\nReward: " + str(data['reward']) + " pansun"
    if data['catchRate'] == 200:
        reply += "\n\n:x: **A catch rate of 200 is the default value. " \
                 "Please double check your input if you got this answer**"

    embed = discord.Embed(title=data['temtem'],
                          description=reply,
                          color=0x00d0f5)
    await ctx.send(embed=embed)


@bot.command()
async def temrewards(ctx):
    pass
    # Currently not getting any data
    url = "https://temtem-api.mael.tech/api/freetem/rewards"
    data = get_json(url)
    print(data)


@bot.command()
async def temquest(ctx, *quest_name):
    url = "https://temtem-api.mael.tech/api/quests"
    data = get_json(url)
    quest = ""
    for word in quest_name:
        quest += word.lower() + " "
    quest = quest.rstrip()

    for i in range(len(data)):
        if data[i]['name'].lower() == quest:
            quest_data = data[i]

            reply = "Wiki url: " + quest_data['wikiUrl']
            reply += "\nType: " + quest_data['type']
            reply += "\n\nsteps: "
            for step in quest_data['steps']:
                reply += "\n" + step
            reply += "\n\nRewards: "
            for reward in quest_data['rewards']:
                reply += "\n" + reward
            reply += "\n\nStarting location: " + quest_data['startingLocation']
            reply += "\nStarting NPC: " + quest_data['startingNPC']
            reply += "\nRequirements: " + quest_data['requirements']

            if len(reply) > 2000:
                reply = reply[:2000]

            embed = discord.Embed(title=quest,
                                  description=reply,
                                  color=0x00d0f5)

            await ctx.send(embed=embed)
            return

    await ctx.send(":x: **Could not find quest**")


@bot.command()
async def tempatch(ctx):
    url = "https://temtem-api.mael.tech/api/patches"
    data = get_json(url)[0]

    reply = "Official patch notes: " + data['url']
    reply += "\nDate: " + data['date']
    reply += "\n(yyyy-mm-dd)"

    embed = discord.Embed(title=data['name'],
                          description=reply,
                          color=0x00d0f5)

    await ctx.send(embed=embed)


@bot.command()
async def temitem(ctx, *item_name):
    item = ""
    for word in item_name:
        item += word.lower() + " "
    item = item.rstrip()

    item_info = get_items(item)

    if not item_info:
        item_info = get_course(item)

    if not item_info:
        item_info = get_gear(item)

    if not item_info:
        await ctx.send(":x: **Could not find \"" + item + "\"**")
        return

    embed = discord.Embed(title=item_info[0],
                          description=item_info[1],
                          color=0x00d0f5)

    await ctx.send(embed=embed)


@bot.command()
async def temgear(ctx, *gear_name):
    gear = ""
    for word in gear_name:
        gear += word.lower() + " "
    gear = gear.rstrip()

    gear_info = get_gear(gear)
    if gear_info:
        embed = discord.Embed(title=gear_info[0],
                              description=gear_info[1],
                              color=0x00d0f5)
        await ctx.send(embed=embed)
    else:
        await ctx.send(":x: **Could not find gear \"" + gear + "\"")


@bot.command()
async def temcourse(ctx, *course_name):
    course = ""
    for word in course_name:
        course += word.lower() + " "
    course = course.rstrip()

    course_info = get_course(course)
    if course_info:
        embed = discord.Embed(title=course_info[0],
                              description=course_info[1],
                              color=0x00d0f5)
        await ctx.send(embed=embed)
    else:
        await ctx.send(":x: **Could not find gear \"" + course + "\"**")


@bot.command()
async def temtrait(ctx, *trait_name):
    trait = ""
    for word in trait_name:
        trait += word.lower() + " "
    trait = trait.rstrip()
    print(trait)

    trait_info = get_trait(trait, True)

    if not trait_info:
        await ctx.send(":x: ***Could not find trait \"" + trait + "\"**")
        return

    embed = discord.Embed(title=trait_info[0],
                          description=trait_info[1],
                          color=0x00d0f5)

    await ctx.send(embed=embed)


@bot.command()
async def tembot(ctx):
    await ctx.send(embed=get_help(discord, bot.command_prefix))


@bot.command()
async def temhelp(ctx):
    await ctx.send(embed=get_help(discord, bot.command_prefix))


@bot.event
async def on_ready():
    print("Bot is online")


bot.run(token)


"""
temtem
- luma, stats, details, techniques/moves, trivia, evolution/evo, locations/loc, traits

freetem
- name, level

temquest
- quest name

tempatch
- returns url to patchnotes

temitem
- item name

temgear
- Can also use temitem

temcourse
- Can also use temitem

temtrait
- returns info about trait
"""